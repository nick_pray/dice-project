import { isArray } from 'lodash';

export default (actions, handler) =>
  (isArray(actions) ? actions : [actions]).reduce(
    (actionMap, action) => ({
      ...actionMap,
      [action]: handler
    }),
    {}
  );
