import { connect } from 'react-redux';

import { actions as gameActions } from '@/store/game';

import Game from './Game.component';

const mapStateToProps = ({ game }) => ({
  playerHasRolled: game.get('playerHasRolled'),
  gameOver: game.get('gameOver'),
  gameOverMessage: game.get('gameOverMessage'),
  currentPlayer: game
    .get('players')
    .find(player => player.get('id') === game.get('currentPlayerId'))
    .toJS()
});

const mapDispatchToProps = dispatch => ({
  onRollDicePress: () => dispatch(gameActions.rollDice()),
  onKeepPress: () => dispatch(gameActions.keepDice()),
  onNewGamePress: () => dispatch(gameActions.startNewGame())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);
