# Dice Game

This is the dice game project for the medbridge interview. Requirements can be found at `docs/requirements.md`.

## Prerequisites

- Node JS (To build the project)
  - Install from https://nodejs.org/en/
- A computer with a web browser (to run the project)
  - Latest version of chrome should be sufficient

## Installing the app

`npm i` _without running this, none of the following commands will work_

## Building the app

`npm build`

`npm run build:watch` to watch for changes

## Running the app

`npm start`

After running start navigate to the link that is given to you in the terminal

## Running tests

`npm test`

## Starting coverage server

After running tests a coverage report is put in `coverage/lcov-report` there is a built-in webserver for

viewing these docs. Start it by running:

`npm run start:coverage`

After starting the coverage server navigate to the link that is given to you in the terminal

## Running linting

`npm run lint`

## Globally formatting code using prettier

`npm run format`

## TODO

- Settings page for building out custom player, dice, dice face and dice Settings
- Reduce image size
- Move images out of repository
- Optimization of sagas
- Finish unit tests
- Create more complex ai options
- Add save/load game feature
- Get release build working
- Repeat the rounds in succession so everyone gets a turn to start
- Document (a.k.a. jsdoc and an architecture overview document)

## Image credits

- [Dice image on home page](https://www.pinclipart.com/pindetail/ihwbo_dice-clip-art-border-clipart-free-clipart-images/)
- [Dice faces](https://commons.wikimedia.org/)
- [Portraits by Azamat Zhanisov](https://unsplash.com/search/photos/portrait)
