import { Map } from 'immutable';
import { connect } from 'react-redux';

import Rules from './Rules.component';

const mapStateToProps = ({ game }) => {
  const dice = game.get('dice').toJS();

  let faces = Map({});

  dice.forEach(die =>
    die.die.faces.forEach(face => {
      faces = faces.set(face.id, face);
    })
  );
  return {
    faces: faces
      .valueSeq()
      .toArray()
      .sort((a, b) => {
        if (a.value === b.value) return 0;
        return a.value < b.value ? -1 : 1;
      })
  };
};

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Rules);
