import { all, put, select, takeEvery } from 'redux-saga/effects';
import { replace } from 'connected-react-router';

import { ROUTES } from '@/components/Router';
import { selectors as gameSelectors } from '@/store/game';

import { GLOBAL_LOCATION_CHANGE } from './router.constants';

function* handleLocationChange({
  payload: {
    location: { pathname }
  }
}) {
  if (pathname === ROUTES.GAME) {
    const gameInProgress = yield select(gameSelectors.currentGameInProgress);
    if (!gameInProgress) yield put(replace(ROUTES.MENU));
  }
}

export default function* routerSaga() {
  yield all([takeEvery(GLOBAL_LOCATION_CHANGE, handleLocationChange)]);
}
