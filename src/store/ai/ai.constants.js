import enumerate from '@/core/enumerate';

export const AI_INTELLIGENCE_LEVEL = enumerate(['BASIC']);
export const AI_ACTION_DELAY = 1500;
export const BASIC_AI_VALUE_THRESHOLD = 2;
