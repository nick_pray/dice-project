import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import { random } from 'lodash';
import { push } from 'connected-react-router';
import { ROUTES } from '@/components/Router';
import uuid from 'uuid/v4';

import { selectors as settingsSelectors } from '@/store/settings';
import { saga as aiSaga } from '@/store/ai';

import * as gameActions from './game.actions';
import * as gameSelectors from './game.selectors';
import { KEEP_STATE } from './game.constants';

function* simulateComputerPlayer() {
  const gameOver = yield select(gameSelectors.gameOver);

  if (gameOver) return;

  yield call(aiSaga);
}

function* loadDice() {
  const diceSet = yield select(settingsSelectors.diceSet);

  yield put(
    gameActions.loadDiceDone(
      diceSet.map(die => {
        return fromJS({
          id: uuid(),
          keep: KEEP_STATE.NO_KEEP,
          currentFaceId: die
            .get('faces')
            .first()
            .get('id'),
          die
        });
      })
    )
  );
}

function* loadPlayers() {
  const players = yield select(settingsSelectors.players);
  yield put(gameActions.loadPlayersDone(players));

  const firstPlayer = players.get(random(players.size - 1));
  yield put(gameActions.setCurrentPlayer(firstPlayer.get('id')));
}

function* initRounds() {
  const rounds = yield select(settingsSelectors.rounds);
  yield put(gameActions.initRounds(rounds));
}

function* sumDiceValues() {
  const dice = yield select(gameSelectors.dice);
  return dice.reduce((sum, die) => {
    const selectedFace = die
      .getIn(['die', 'faces'])
      .find(face => face.get('id') === die.get('currentFaceId'));
    return sum + selectedFace.get('value');
  }, 0);
}

function* getNextPlayerId() {
  const currentPlayer = yield select(gameSelectors.currentPlayer);
  const players = yield select(gameSelectors.players);

  const currentPlayerIndex = players.findIndex(
    player => player.get('id') === currentPlayer.get('id')
  );

  if (currentPlayerIndex + 1 > players.size - 1) return players.first().get('id');

  return players.get(currentPlayerIndex + 1).get('id');
}

function* moveToNextPlayer() {
  const nextPlayerId = yield call(getNextPlayerId);

  yield call(loadDice);

  yield put(gameActions.moveToNextPlayerDone(nextPlayerId));

  const currentPlayer = yield select(gameSelectors.currentPlayer);

  if (currentPlayer.get('isComputer')) {
    yield call(simulateComputerPlayer);
  }
}

function* handleStartNewGame() {
  yield put(gameActions.startNewGameBegin());

  yield put(push(ROUTES.GAME));

  yield call(loadDice);
  yield call(loadPlayers);
  yield call(initRounds);

  yield put(gameActions.startNewGameEnd());

  const currentPlayer = yield select(gameSelectors.currentPlayer);

  if (currentPlayer.get('isComputer')) {
    yield call(simulateComputerPlayer);
  }
}

function* handleRollDice() {
  const playerHasRolled = yield select(gameSelectors.playerHasRolled);
  const dice = yield select(gameSelectors.dice);

  if (playerHasRolled && !dice.find(die => die.get('keep') === KEEP_STATE.PENDING_KEEP)) {
    yield put(gameActions.rollError('You must select at least one die'));
    return;
  }

  const rolledDice = dice.map(die => {
    if (die.get('keep') === KEEP_STATE.KEEP) return die;
    if (die.get('keep') === KEEP_STATE.PENDING_KEEP) return die.set('keep', KEEP_STATE.KEEP);

    const newFaceIndex = random(die.getIn(['die', 'faces']).size - 1);

    return die.set('currentFaceId', die.getIn(['die', 'faces', newFaceIndex, 'id']));
  });

  yield put(gameActions.rollDiceComplete(rolledDice));
}

function* handleKeepDice() {
  const diceValues = yield call(sumDiceValues);
  yield put(gameActions.recordScore(diceValues));
  yield call(moveToNextPlayer);
}

export default function* gameSaga() {
  yield all([
    takeLatest(gameActions.TYPES.START_NEW_GAME, handleStartNewGame),
    takeLatest(gameActions.TYPES.ROLL_DICE, handleRollDice),
    takeLatest(gameActions.TYPES.KEEP_DICE, handleKeepDice)
  ]);
}
