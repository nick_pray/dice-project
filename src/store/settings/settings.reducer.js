import { fromJS } from 'immutable';

import Reducer from '@/core/Reducer';

import { DEFAULT_ROUNDS, DEFAULT_FACES, DEFAULT_DIE, DEFAULT_PLAYERS } from './settings.constants';

const INIT_STATE = fromJS({
  rounds: DEFAULT_ROUNDS,
  faces: DEFAULT_FACES,
  dice: [DEFAULT_DIE],
  diceSet: Array(5).fill(DEFAULT_DIE),
  players: DEFAULT_PLAYERS
});

export default Reducer({}, INIT_STATE);
