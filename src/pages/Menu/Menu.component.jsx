import React from 'react';
import PropTypes from 'prop-types';

import diceImage from '@/assets/dice.png';

import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import styles from './Menu.styles';

const Menu = ({ onStartNewGamePress, onContinueGamePress, gameInProgress }) => (
  <Container style={{ display: 'flex', flexDirection: 'column' }}>
    <Grid container spacing={3}>
      <Grid item xs={12} style={styles.menuItemGroup}>
        <Typography variant="h1">The Dice Game</Typography>
      </Grid>

      <Grid item xs={12} style={{ ...styles.menuItemGroup, ...styles.imageGroup }}>
        <img alt="Dice Game" style={styles.diceGameImage} src={diceImage} />
      </Grid>

      <Grid
        container
        item
        xs={12}
        spacing={3}
        style={{ ...styles.menuItemGroup, ...styles.buttonGroup }}
      >
        <Grid item xs={12} style={styles.menuItemGroup}>
          <Button variant="contained" onClick={onStartNewGamePress}>
            Start New Game
          </Button>
        </Grid>

        <Grid item xs={12} style={styles.menuItemGroup}>
          <Button variant="contained" disabled={!gameInProgress} onClick={onContinueGamePress}>
            Continue Game
          </Button>
        </Grid>
      </Grid>
    </Grid>
  </Container>
);

Menu.propTypes = {
  onStartNewGamePress: PropTypes.func.isRequired,
  gameInProgress: PropTypes.bool.isRequired,
  onContinueGamePress: PropTypes.func.isRequired
};

export default Menu;
