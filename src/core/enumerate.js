export default (keys, prefix = '') =>
  keys.reduce(
    (enumerations, key) => ({
      ...enumerations,
      [key]: `${prefix}${key}`
    }),
    {}
  );
