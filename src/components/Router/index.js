import Router from './Router.component';

export { ROUTES } from './Router.constants';

export default Router;
