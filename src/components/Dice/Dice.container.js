import { connect } from 'react-redux';

import { actions as gameActions } from '@/store/game';

import Dice from './Dice.component';

const mapStateToProps = ({ game }) => ({
  dice: game.get('dice').toJS(),
  playerHasRolled: game.get('playerHasRolled'),
  rollErrors: game.get('rollErrors').toJS(),
  gameOver: game.get('gameOver')
});

const mapDispatchToProps = dispatch => ({
  onDiePress: dieId => dispatch(gameActions.toggleDieKeepState(dieId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dice);
