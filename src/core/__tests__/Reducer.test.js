import Reducer from '../Reducer';

const { describe, expect, it } = global;

describe('Reducer(handlers, initState)', () => {
  it('initializes state to {}', () => {
    const reducer = Reducer({});
    expect(reducer()).toEqual({});
  });

  it('creates reducer that can be initialized', () => {
    const mockState = { some: 'state' };
    const reducer = Reducer({}, mockState);
    expect(reducer()).toEqual(mockState);
  });

  it('creates reducer that can handle action', () => {
    const mockState = { some: 'state' };
    const mockHandler = state => ({ ...state, some: 'updated' });
    const mockHandlerKey = 'UPDATE_SOME';
    const reducer = Reducer({ [mockHandlerKey]: mockHandler }, mockState);
    expect(reducer(mockState, { type: mockHandlerKey })).toEqual({ some: 'updated' });
  });
});
