import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import { KEEP_STATE } from '@/pages/Game/Game.constants';

const ENTER_KEY_CODE = 13;

const useStyles = makeStyles(() => ({
  container: {
    borderRadius: '10px'
  },
  keepContainer: {
    backgroundColor: 'rgba(0,0,0,0.2)'
  },
  pendingKeepContainer: {
    backgroundColor: 'rgba(0, 102, 255,0.4)'
  },
  image: {
    width: 100,
    display: 'block'
  },
  keepImage: {
    opacity: 0.6
  }
}));

function Die({ die, visibleFaceId, keep, onPress }) {
  const styles = useStyles();
  const currentFace = die.faces.find(({ id }) => id === visibleFaceId);

  const handleKeyUp = e => e.which === ENTER_KEY_CODE && onPress();

  return (
    <div
      onClick={onPress}
      onKeyUp={handleKeyUp}
      role="button"
      tabIndex={0}
      className={[
        styles.container,
        ...(keep === KEEP_STATE.KEEP ? [styles.keepContainer] : []),
        ...(keep === KEEP_STATE.PENDING_KEEP ? [styles.pendingKeepContainer] : [])
      ].join(' ')}
    >
      <img
        alt={currentFace.image}
        src={currentFace.image}
        className={[styles.image, ...(keep === KEEP_STATE.KEEP ? [styles.keepImage] : [])].join(
          ' '
        )}
      />
    </div>
  );
}

Die.propTypes = {
  die: PropTypes.shape({
    faces: PropTypes.arrayOf(PropTypes.shape({ image: PropTypes.string }))
  }).isRequired,
  visibleFaceId: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  keep: PropTypes.string.isRequired
};

export default Die;
