import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import Immutable from 'immutable';
import createSagaMiddleware from 'redux-saga';

import Router from '@/components/Router';
import { reducer, saga } from '@/store';

const immutableStateTransformer = state =>
  Object.keys(state).reduce(
    (newState, key) => ({
      ...newState,
      [key]: Immutable.Iterable.isIterable(state[key]) ? state[key].toJS() : state[key]
    }),
    {}
  );

const logger = createLogger({
  stateTransformer: immutableStateTransformer
});

const sagaMiddleware = createSagaMiddleware();

const history = createBrowserHistory();

const store = createStore(
  reducer(history),
  applyMiddleware(logger, routerMiddleware(history), sagaMiddleware)
);

sagaMiddleware.run(saga);

const App = () => (
  <Provider {...{ store }}>
    <Router history={history} />
  </Provider>
);

export const $testing = {
  immutableStateTransformer
};

export default App;
