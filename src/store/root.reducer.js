import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { reducer as settings } from './settings';
import { reducer as game } from './game';

export default history =>
  combineReducers({
    router: connectRouter(history),
    settings,
    game
  });
