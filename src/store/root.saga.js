import { all, call } from 'redux-saga/effects';

import { saga as game } from './game';
import { saga as router } from './router';

export default function*() {
  yield all([call(game), call(router)]);
}
