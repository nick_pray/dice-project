import React from 'react';
import PropTypes from 'prop-types';
import { sum } from 'lodash';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';

import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  currentPlayerItem: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderRadius: '10px'
  },
  roundIndicator: {
    marginBottom: '10px'
  }
}));

function ScoreBoard({ currentPlayerId, players, scores, currentDiceScore, rounds, currentRound }) {
  const classes = useStyles();

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" className={classes.roundIndicator}>
          {`Round: ${currentRound}/${rounds}`}
        </Typography>
        <Typography variant="h5" component="h2">
          Players
        </Typography>
        <List>
          {players.map(player => {
            const playerName = `${player.firstName} ${player.lastName}`;
            const { [player.id]: playerScores = [] } = scores;
            const isCurrentPlayer = player.id === currentPlayerId;
            return (
              <ListItem
                key={playerName}
                alignItems="flex-start"
                className={[...(isCurrentPlayer ? [classes.currentPlayerItem] : [])].join(' ')}
              >
                <ListItemAvatar>
                  <Avatar alt={playerName} src={player.image} />
                </ListItemAvatar>
                <ListItemText
                  primary={`${playerName}${player.isComputer ? ' (AI)' : ''}`}
                  secondary={`Score: ${sum([
                    ...playerScores,
                    ...(isCurrentPlayer ? [currentDiceScore] : [])
                  ])}`}
                />
              </ListItem>
            );
          })}
        </List>
      </CardContent>
    </Card>
  );
}

ScoreBoard.propTypes = {
  currentPlayerId: PropTypes.string.isRequired,
  players: PropTypes.arrayOf(PropTypes.object).isRequired,
  scores: PropTypes.object.isRequired,
  currentDiceScore: PropTypes.number.isRequired
};

export default ScoreBoard;
