import React from 'react';
import { Route } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import PropTypes from 'prop-types';

import Menu from '@/pages/Menu';
import Game from '@/pages/Game';

import { ROUTES } from './Router.constants';

const Router = ({ history }) => (
  <ConnectedRouter history={history}>
    <React.Fragment>
      <Route exact path={ROUTES.MENU} component={Menu} />
      <Route exact path={ROUTES.GAME} component={Game} />
    </React.Fragment>
  </ConnectedRouter>
);

Router.propTypes = {
  history: PropTypes.object.isRequired
};

export default Router;
