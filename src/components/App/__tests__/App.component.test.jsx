import React from 'react';
import { fromJS } from 'immutable';
import { shallow } from 'enzyme';

import App, { $testing } from '../App.component';

const { beforeEach, describe, expect, it } = global;

const { immutableStateTransformer } = $testing;

describe('<App />', () => {
  let wrapper;

  const mockProps = {};

  beforeEach(() => {
    wrapper = shallow(<App {...mockProps} />);
  });

  it('matches snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe('immutableStateTransformer(state)', () => {
  it('converts immutable state to regular js types', () => {
    const mockState = {
      storeSection: fromJS({
        some: {
          nested: 'state'
        }
      })
    };

    const expected = {
      storeSection: { some: { nested: 'state' } }
    };

    expect(immutableStateTransformer(mockState)).toEqual(expected);
  });

  it('still works with regular js types', () => {
    const mockState = {
      storeSection: {
        some: {
          nested: 'state'
        }
      }
    };

    const expected = {
      storeSection: { some: { nested: 'state' } }
    };

    expect(immutableStateTransformer(mockState)).toEqual(expected);
  });
});
