import { fromJS } from 'immutable';
import { clamp, sum } from 'lodash';

import Reducer from '@/core/Reducer';

import { TYPES } from './game.actions';
import { KEEP_STATE } from './game.constants';

const INIT_STATE = fromJS({
  loading: true,
  inProgress: false,
  playerHasRolled: false,
  currentPlayerId: null,
  rounds: null,
  currentRound: null,
  gameOver: false,
  gameOverMessage: null,
  dice: [],
  players: [],
  scores: {},
  rollErrors: []
});

export default Reducer(
  {
    [TYPES.START_NEW_GAME_BEGIN]: () => INIT_STATE.set('inProgress', true).set('loading', true),
    [TYPES.START_NEW_GAME_END]: state => state.set('loading', false),
    [TYPES.LOAD_DICE_DONE]: (state, { payload: dice }) => state.set('dice', dice),
    [TYPES.LOAD_PLAYERS_DONE]: (state, { payload: players }) => state.set('players', players),
    [TYPES.INIT_ROUNDS]: (state, { payload: rounds }) =>
      state.set('rounds', rounds).set('currentRound', 1),
    [TYPES.SET_CURRENT_PLAYER]: (state, { payload: currentPlayerId }) =>
      state.set('currentPlayerId', currentPlayerId),
    [TYPES.TOGGLE_DIE_KEEP_STATE]: (state, { payload: dieId }) => {
      return state
        .updateIn(['dice'], dice => {
          return dice.map(die => {
            if (die.get('id') !== dieId) return die;
            if (die.get('keep') === KEEP_STATE.KEEP) return die;
            return die.set(
              'keep',
              die.get('keep') === KEEP_STATE.NO_KEEP ? KEEP_STATE.PENDING_KEEP : KEEP_STATE.NO_KEEP
            );
          });
        })
        .set('rollErrors', fromJS([]));
    },
    [TYPES.ROLL_DICE_COMPLETE]: (state, { payload: dice }) =>
      state.set('dice', dice).set('playerHasRolled', true),
    [TYPES.ROLL_ERROR]: (state, { payload: error }) => state.set('rollErrors', fromJS([error])),
    [TYPES.RECORD_SCORE]: (state, { payload: score }) =>
      state.updateIn(['scores', state.get('currentPlayerId')], scores =>
        (scores || fromJS([])).merge([score])
      ),
    [TYPES.MOVE_TO_NEXT_PLAYER_DONE]: (state, { payload: playerId }) => {
      const scoreLengths = state
        .get('scores')
        .map(scores => scores.size)
        .toList()
        .toJS();

      const smallestScoreLength = (scoreLengths.length >= state.get('players').size
        ? scoreLengths
        : [...scoreLengths, ...Array(state.get('players').size).fill(0)]
      ).reduce(
        (smallestLength, length) => (length < smallestLength ? length : smallestLength),
        state.get('rounds')
      );

      let newState = state
        .set('currentPlayerId', playerId)
        .set('playerHasRolled', false)
        .set('currentRound', clamp(smallestScoreLength + 1, 1, state.get('rounds')));

      const gameOver = scoreLengths.every(length => length >= state.get('rounds'));
      if (gameOver) {
        const lowestScore = state.get('scores').reduce((lowScore, score) => {
          const scoreSum = sum(score.toJS());
          if (lowScore === null || scoreSum < lowScore) return scoreSum;
          return lowScore;
        }, null);

        const lowestScorePlayers = state
          .get('players')
          .filter(player => sum(state.getIn(['scores', player.get('id')]).toJS()) === lowestScore);

        const gameOverMessage = `${lowestScorePlayers
          .map(player => `${player.get('firstName')} ${player.get('lastName')}`)
          .toArray()
          .join(', ')}
          wins!
          `;

        newState = newState.set('gameOver', true).set('gameOverMessage', gameOverMessage);
      }

      return newState;
    },
    [TYPES.KEEP_DICE]: state => state.set('rollErrors', fromJS([])),
    [TYPES.ROLL_DICE]: state => state.set('rollErrors', fromJS([]))
  },
  INIT_STATE
);
