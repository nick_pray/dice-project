export default (action, customPayloadAdapter) => {
  return (firstArg, ...restOfArgs) => ({
    type: action,
    payload: customPayloadAdapter ? customPayloadAdapter(firstArg, ...restOfArgs) : firstArg
  });
};
