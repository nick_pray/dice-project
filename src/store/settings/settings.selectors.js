export const diceSet = ({ settings }) => settings.get('diceSet');
export const players = ({ settings }) => settings.get('players');
export const rounds = ({ settings }) => settings.get('rounds');
