import { call, delay, put, select, take } from 'redux-saga/effects';
import { fromJS } from 'immutable';

import { selectors as gameSelectors, actions as gameActions } from '@/store/game';
import { KEEP_STATE } from '@/store/game/game.constants';

import { AI_ACTION_DELAY, BASIC_AI_VALUE_THRESHOLD } from './ai.constants';

const getDieValue = die =>
  die
    .getIn(['die', 'faces'])
    .find(face => face.get('id') === die.get('currentFaceId'))
    .get('value');

function* basicAI() {
  yield delay(AI_ACTION_DELAY);

  let diceToRoll;

  do {
    yield put(gameActions.rollDice());
    yield take(gameActions.TYPES.ROLL_DICE_COMPLETE);

    yield delay(AI_ACTION_DELAY);

    const noKeepDice = yield select(gameSelectors.diceByKeepState([KEEP_STATE.NO_KEEP]));

    const diceBelowThreshold = noKeepDice.filter(die => {
      return getDieValue(die) <= BASIC_AI_VALUE_THRESHOLD;
    });

    if (diceBelowThreshold.size < 1) {
      const lowestDice = noKeepDice.reduce((lowDie, die) => {
        if (lowDie === null || getDieValue(die) < getDieValue(lowDie)) {
          return die;
        }

        return lowDie;
      }, null);

      diceToRoll = fromJS([lowestDice]);
    } else {
      diceToRoll = diceBelowThreshold;
    }

    if (diceToRoll.size === noKeepDice.size) {
      yield put(gameActions.keepDice());
      return;
    }

    for (let i = 0; i < diceToRoll.size; i += 1) {
      yield put(gameActions.toggleDieKeepState(diceToRoll.getIn([i, 'id'])));
      yield delay(AI_ACTION_DELAY);
    }
  } while (diceToRoll.size > 0);
}

export default function* aiSaga() {
  const currentPlayer = yield select(gameSelectors.currentPlayer);

  switch (currentPlayer.get('intelligence')) {
    default:
      // BASIC
      yield call(basicAI);
      break;
  }
}
