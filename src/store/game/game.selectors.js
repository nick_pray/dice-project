export const allSettings = ({ settings }) => settings;
export const currentPlayer = ({ game, settings }) =>
  settings.get('players').find(player => player.get('id') === game.get('currentPlayerId'));
export const currentGameInProgress = ({ game }) => game.get('inProgress');
export const dice = ({ game }) => game.get('dice');
export const diceByKeepState = keepStates => ({ game }) =>
  game.get('dice').filter(die => keepStates.includes(die.get('keep')));
export const playerHasRolled = ({ game }) => game.get('playerHasRolled');
export const players = ({ game }) => game.get('players');
export const gameOver = ({ game }) => game.get('gameOver');
export const scores = ({ game }) => game.get('scores');
