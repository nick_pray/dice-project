import React from 'react';
import { shallow } from 'enzyme';

import Game from '../Game.component';

const { beforeEach, describe, expect, it } = global;

describe('<Game />', () => {
  let wrapper;

  const mockProps = {};

  beforeEach(() => {
    wrapper = shallow(<Game {...mockProps} />);
  });

  it('matches snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
