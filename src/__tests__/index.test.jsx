import React from 'react';
import ReactDOM from 'react-dom';
import App from '@/components/App';

const { expect, describe, it } = global;

jest.mock('react-dom', () => ({
  render: jest.fn()
}));

global.document.getElementById = jest.fn((...args) => global.document.getElementById(...args));

describe('index.js', () => {
  // Tests go here
  it('calls render for app', () => {
    const mockReturn = 'some_element';
    global.document.getElementById.mockReturnValueOnce(mockReturn);
    jest.requireActual('../index');
    expect(ReactDOM.render).toHaveBeenCalledWith(<App />, mockReturn);
  });
});
