import enumerate from '@/core/enumerate';
import actionCreator from '@/core/actionCreator';

const ACTION_PREFIX = 'GAME_';

export const TYPES = enumerate(
  [
    'START_NEW_GAME',
    'START_NEW_GAME_BEGIN',
    'START_NEW_GAME_END',
    'LOAD_DICE_DONE',
    'LOAD_PLAYERS_DONE',
    'INIT_ROUNDS',
    'ROLL_DICE',
    'SUBMIT_SCORE',
    'SET_CURRENT_PLAYER',
    'TOGGLE_DIE_KEEP_STATE',
    'ROLL_DICE_COMPLETE',
    'ROLL_ERROR',
    'KEEP_DICE',
    'RECORD_SCORE',
    'MOVE_TO_NEXT_PLAYER_DONE'
  ],
  ACTION_PREFIX
);

export const startNewGame = actionCreator(TYPES.START_NEW_GAME);
export const startNewGameBegin = actionCreator(TYPES.START_NEW_GAME_BEGIN);
export const startNewGameEnd = actionCreator(TYPES.START_NEW_GAME_END);

export const rollDice = actionCreator(TYPES.ROLL_DICE);

export const loadDiceDone = actionCreator(TYPES.LOAD_DICE_DONE);
export const loadPlayersDone = actionCreator(TYPES.LOAD_PLAYERS_DONE);
export const initRounds = actionCreator(TYPES.INIT_ROUNDS);
export const setCurrentPlayer = actionCreator(TYPES.SET_CURRENT_PLAYER);
export const toggleDieKeepState = actionCreator(TYPES.TOGGLE_DIE_KEEP_STATE);
export const rollDiceComplete = actionCreator(TYPES.ROLL_DICE_COMPLETE);
export const rollError = actionCreator(TYPES.ROLL_ERROR);
export const keepDice = actionCreator(TYPES.KEEP_DICE);
export const recordScore = actionCreator(TYPES.RECORD_SCORE);
export const moveToNextPlayerDone = actionCreator(TYPES.MOVE_TO_NEXT_PLAYER_DONE);
