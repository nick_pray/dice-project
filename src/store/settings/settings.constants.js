import diceFace1 from '@/assets/dice-1-face.png';
import diceFace2 from '@/assets/dice-2-face.png';
import diceFace3 from '@/assets/dice-3-face.png';
import diceFace4 from '@/assets/dice-4-face.png';
import diceFace5 from '@/assets/dice-5-face.png';
import diceFace6 from '@/assets/dice-6-face.png';

import albertPortrait from '@/assets/albert-dera.jpg';
import azamatPortrait from '@/assets/azamat-zhanisov.jpg';
import juricaPortrait from '@/assets/jurica-koletic.jpg';
import oriolPortrait from '@/assets/oriol-casas.jpg';

import { AI_INTELLIGENCE_LEVEL } from '@/store/ai/ai.constants';

export const DEFAULT_ROUNDS = 4;

export const DEFAULT_FACES = [diceFace1, diceFace2, diceFace3, diceFace4, diceFace5, diceFace6];

export const DEFAULT_DIE = {
  id: '-1',
  name: '6 Sided Die',
  faces: [
    { id: '-1', value: 1, image: diceFace1 },
    { id: '-2', value: 2, image: diceFace2 },
    { id: '-3', value: 3, image: diceFace3 },
    { id: '-4', value: 0, image: diceFace4 },
    { id: '-5', value: 5, image: diceFace5 },
    { id: '-6', value: 6, image: diceFace6 }
  ]
};

const PLAYER_ALBERT = {
  id: '-1',
  firstName: 'Albert',
  lastName: 'Dera',
  isComputer: true,
  intelligence: AI_INTELLIGENCE_LEVEL.BASIC,
  image: albertPortrait
};

const PLAYER_AZAMAT = {
  id: '-2',
  firstName: 'Azamat',
  lastName: 'Zhanisov',
  isComputer: true,
  intelligence: AI_INTELLIGENCE_LEVEL.BASIC,
  image: azamatPortrait
};

const PLAYER_JURICA = {
  id: '-3',
  firstName: 'Jurica',
  lastName: 'Koletic',
  isComputer: true,
  intelligence: AI_INTELLIGENCE_LEVEL.BASIC,
  image: juricaPortrait
};

const PLAYER_ORIOL = {
  id: '-4',
  firstName: 'Oriol',
  lastName: 'Casas',
  isComputer: true,
  intelligence: AI_INTELLIGENCE_LEVEL.BASIC,
  image: oriolPortrait
};

export const DEFAULT_PLAYERS = [PLAYER_ALBERT, PLAYER_AZAMAT, PLAYER_JURICA, PLAYER_ORIOL];
