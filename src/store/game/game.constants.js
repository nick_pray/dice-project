import enumerate from '@/core/enumerate';

export const KEEP_STATE = enumerate(['NO_KEEP', 'PENDING_KEEP', 'KEEP']);
