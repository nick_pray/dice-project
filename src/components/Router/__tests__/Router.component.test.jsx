import React from 'react';
import { shallow } from 'enzyme';

import Router from '../Router.component';

const { beforeEach, describe, expect, it } = global;

describe('<Router />', () => {
  let wrapper;

  const mockProps = {};

  beforeEach(() => {
    wrapper = shallow(<Router {...mockProps} />);
  });

  it('matches snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
