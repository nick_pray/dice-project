import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import Die from '@/components/Die';

const useStyles = makeStyles(() => ({
  diceContainer: {
    display: 'flex',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    position: 'relative'
  },
  error: {
    color: '#ff3333',
    marginTop: '10px'
  },
  rollCover: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
}));

function Dice({ cardClassName, dice, onDiePress, playerHasRolled, rollErrors, gameOver }) {
  const classes = useStyles();
  return (
    <Card className={cardClassName}>
      <CardContent>
        <div className={classes.diceContainer}>
          {!playerHasRolled && (
            <div className={classes.rollCover}>
              <Typography className={classes.rollCoverText}>
                {gameOver ? 'Game Over' : 'Press ROLL to begin'}
              </Typography>
            </div>
          )}
          {dice.map(({ id, currentFaceId, keep, die }) => (
            <Die
              key={id}
              die={die}
              visibleFaceId={currentFaceId}
              keep={keep}
              onPress={() => playerHasRolled && onDiePress(id)}
            />
          ))}
        </div>
        {rollErrors.map(error => (
          <Typography key={error} className={classes.error}>
            {error}
          </Typography>
        ))}
      </CardContent>
    </Card>
  );
}

Dice.propTypes = {
  cardClassName: PropTypes.string,
  dice: PropTypes.array.isRequired,
  onDiePress: PropTypes.func.isRequired,
  playerHasRolled: PropTypes.bool.isRequired,
  rollErrors: PropTypes.array.isRequired,
  gameOver: PropTypes.bool.isRequired
};

Dice.defaultProps = {
  cardClassName: ''
};

export default Dice;
