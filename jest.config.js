module.exports = {
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.{js,jsx}'],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },
  moduleNameMapper: {
    '\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>__mocks__/fileMock.js',
    '@/(.*)$': '<rootDir>/src/$1'
  },
  setupFiles: ['<rootDir>/jest/enzyme.config', '<rootDir>/jest/babel.config'],
  verbose: true
};
