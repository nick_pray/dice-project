import { connect } from 'react-redux';

import { KEEP_STATE } from '@/store/game/game.constants';

import ScoreBoard from './ScoreBoard.component';

const mapStateToProps = ({ game }) => ({
  rounds: game.get('rounds'),
  currentRound: game.get('currentRound'),
  currentPlayerId: game.get('currentPlayerId'),
  players: game.get('players').toJS(),
  scores: game.get('scores').toJS(),
  currentDiceScore: game.get('dice').reduce((sum, die) => {
    if (die.get('keep') === KEEP_STATE.KEEP) {
      const selectedFaceIndex = die
        .getIn(['die', 'faces'])
        .findIndex(face => face.get('id') === die.get('currentFaceId'));
      return sum + die.getIn(['die', 'faces', selectedFaceIndex, 'value']);
    }
    return sum;
  }, 0)
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScoreBoard);
