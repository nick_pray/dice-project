import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { actions as gameActions } from '@/store/game';
import { ROUTES } from '@/components/Router';

import Menu from './Menu.component';

const mapStateToProps = ({ game }) => ({
  gameInProgress: game.get('inProgress')
});

const mapDispatchToProps = dispatch => ({
  onStartNewGamePress: () => dispatch(gameActions.startNewGame()),
  onContinueGamePress: () => dispatch(push(ROUTES.GAME))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Menu);
