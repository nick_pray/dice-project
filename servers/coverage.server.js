const express = require('express');
const fs = require('fs');
const path = require('path');
const serverConfig = require('./config.json').coverage;

const app = express();

const COVERGE_DIRECTORY = path.join(__dirname, '../coverage');

app.get('/*', (req, res) => {
  if (!fs.existsSync(COVERGE_DIRECTORY))
    res.send('No coverage report exists, collect coverage by running `npm test`.');

  const targetPath = path.join(
    COVERGE_DIRECTORY,
    `lcov-report/${req.path === '/' ? 'index.html' : req.path}`
  );

  res.sendFile(targetPath);
});

app.listen(serverConfig.port, () => {
  console.log(`Coverage Server Listening @ http://localhost:${serverConfig.port}`);
});
