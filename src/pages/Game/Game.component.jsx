import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import ScoreBoard from '@/components/ScoreBoard';
import Rules from '@/components/Rules';
import Dice from '@/components/Dice';

const useStyles = makeStyles(theme => ({
  diceContainer: {
    display: 'flex',
    justifyContent: 'space-around',
    flexWrap: 'wrap'
  },
  actionButton: {
    margin: theme.spacing(1)
  },
  gameBodyCard: {
    marginBottom: theme.spacing(1)
  },
  input: {
    display: 'none'
  },
  currentPlayerItem: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderRadius: '10px'
  },
  error: {
    marginTop: '10px',
    color: '#ff6666'
  }
}));

function Game({
  onRollDicePress,
  onKeepPress,
  playerHasRolled,
  gameOver,
  gameOverMessage,
  onNewGamePress,
  currentPlayer
}) {
  const classes = useStyles();

  return (
    <Container>
      <Grid container spacing={3}>
        <Grid item xs={12} md={4}>
          <ScoreBoard />
        </Grid>

        <Grid item xs={12} md={8}>
          <Rules cardClassName={classes.gameBodyCard} />
          <Dice cardClassName={classes.gameBodyCard} />

          <Card className={classes.gameBodyCard}>
            <CardContent>
              <Button
                variant="contained"
                color="primary"
                onClick={onRollDicePress}
                className={classes.actionButton}
                disabled={gameOver || currentPlayer.isComputer}
              >
                Roll
              </Button>

              <Button
                variant="contained"
                onClick={onKeepPress}
                className={classes.actionButton}
                disabled={!playerHasRolled || gameOver || currentPlayer.isComputer}
              >
                Keep
              </Button>
              {gameOver && (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={onNewGamePress}
                  className={classes.actionButton}
                >
                  New Game
                </Button>
              )}
              {gameOverMessage && <Typography>{gameOverMessage}</Typography>}
              {!gameOver && currentPlayer.isComputer && (
                <Typography>Computer player turn in progress</Typography>
              )}
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}

Game.propTypes = {
  onRollDicePress: PropTypes.func.isRequired,
  onKeepPress: PropTypes.func.isRequired,
  playerHasRolled: PropTypes.bool.isRequired,
  gameOver: PropTypes.bool.isRequired,
  gameOverMessage: PropTypes.string,
  onNewGamePress: PropTypes.func.isRequired,
  currentPlayer: PropTypes.object.isRequired
};

Game.defaultProps = {
  gameOverMessage: null
};

export default Game;
