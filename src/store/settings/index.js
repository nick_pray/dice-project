import * as selectors from './settings.selectors';

export { selectors };
export { default as reducer } from './settings.reducer';
