import React from 'react';
import { shallow } from 'enzyme';

import Menu from '../Menu.component';

const { beforeEach, describe, expect, it } = global;

describe('<Menu />', () => {
  let wrapper;

  const mockProps = {};

  beforeEach(() => {
    wrapper = shallow(<Menu {...mockProps} />);
  });

  it('matches snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
