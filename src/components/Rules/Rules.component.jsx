import React from 'react';
import PropTypes from 'prop-types';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import Typography from '@material-ui/core/Typography';

function Rules({ cardClassName, faces }) {
  return (
    <Card className={cardClassName}>
      <CardContent>
        <Typography variant="h5">Welcome to The Dice Game</Typography>
        <Typography variant="subtitle1">Rules of the game:</Typography>
        <Typography variant="body1">○ A player is chosen at random to start the game.</Typography>
        <Typography variant="body1">
          ○ Each player takes turns rolling the dice by pressing ROLL.
        </Typography>
        <Typography variant="body1">
          ○ After a roll, the current player will select how many dice they want to keep. The player
          must keep at least one of the dice that were just rolled.
        </Typography>
        <Typography variant="body1">
          ○ The player will continue to roll and keep in this manner until all dice have been kept.
          The player may also choose give up remaining rolls if they like the current dice by
          pressing KEEP.
        </Typography>
        <Typography variant="body1">
          ○ After the KEEP button is pressed, the current players turn is over.
        </Typography>
        <Typography variant="body1">
          ○ The players will continue to take turns until all rounds have been completed. The player
          with the lowest score wins.
        </Typography>
        <Typography>○ The dice rolls are worth the following:</Typography>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-around',
            marginTop: '15px',
            flexWrap: 'wrap'
          }}
        >
          {faces.map(face => (
            <div key={face.id}>
              <div>
                <img src={face.image} alt={face.image} style={{ width: 50 }} />
              </div>
              value:
              {face.value}
            </div>
          ))}
        </div>
      </CardContent>
    </Card>
  );
}

Rules.propTypes = {
  cardClassName: PropTypes.string,
  faces: PropTypes.arrayOf(PropTypes.object).isRequired
};

Rules.defaultProps = {
  cardClassName: ''
};

export default Rules;
