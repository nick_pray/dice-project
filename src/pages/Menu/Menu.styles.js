import { createStyles } from '@material-ui/styles';

export default createStyles({
  menuItemGroup: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    display: 'flex'
  },
  diceGameImage: {
    height: 250,
    marginBottom: 24,
    marginTop: 24
  },
  imageGroup: {
    background: '#e6e6e6'
  },
  buttonGroup: {
    marginTop: 24
  }
});
